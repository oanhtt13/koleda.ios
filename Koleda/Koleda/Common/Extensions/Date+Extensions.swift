//
//  Date+Extensions.swift
//  Koleda
//
//  Created by Oanh tran on 8/29/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import Foundation

extension Date {
    
    public static var fm_hhmma                = "hh:mm a"
    public static var fm_HHmm                = "HH:mm"
    
    func fomartAMOrPm() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm:ss a"
        return formatter.string(from: self)
    }
    
    func adding(seconds: Int) -> Date {
        guard let newTime = Calendar.current.date(byAdding: .second, value: seconds, to: self) else {
            return Date()
        }
        return newTime
    }
    
    public init(str: String, format: String, localized: Bool = true) {
        let fmt = DateFormatter()
        fmt.dateFormat = format
        fmt.timeZone = localized ? TimeZone.current : TimeZone(abbreviation: "UTC")
        if let date = fmt.date(from: str) {
            self.init(timeInterval: 0, since: date)
        } else {
            self.init(timeInterval: 0, since: Date())
        }
    }
    
    public func toString(format: String, localized: Bool = true) -> String {
        let fmt = DateFormatter()
        fmt.dateFormat = format
        fmt.timeZone = localized ? TimeZone.current : TimeZone(abbreviation: "UTC")
        return fmt.string(from: self)
    }
}

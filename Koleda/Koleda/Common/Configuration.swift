//
//  Configuration.swift
//  Koleda
//
//  Created by Oanh tran on 7/1/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import Foundation
import Alamofire

extension URLRequest {
    
    static func postRequestWithJsonBody(url: URL, parameters: [String: Any]) -> URLRequest? {
        var urlRequest: URLRequest? = nil
        var bodyData: Data? = nil
        do {
            urlRequest = try URLRequest(url: url, method: .post, headers: ["Content-Type":"application/json"])
            bodyData = try JSONSerialization.data(withJSONObject: parameters)
        } catch {
            return nil
        }
        urlRequest?.httpBody = bodyData
        return urlRequest
    }
    
    static func requestWithJsonBody(url: URL, method: HTTPMethod, parameters: [String: Any]) -> URLRequest? {
        var urlRequest: URLRequest? = nil
        var bodyData: Data? = nil
        do {
            urlRequest = try URLRequest(url: url, method: method, headers: ["Content-Type":"application/json"])
            bodyData = try JSONSerialization.data(withJSONObject: parameters)
        } catch {
            return nil
        }
        urlRequest?.httpBody = bodyData
        return urlRequest
    }
}



class UrlConfigurator {
    
    private class func isProduction() -> Bool {
        guard let info = Bundle.main.infoDictionary, let isProduction = info["BuildProduction"] as? Bool else {
            assert(false, "Failed while reading Build Production from settings")
            return false
        }
        return isProduction
    }
    
    private class func baseUrlString() -> String {
        guard let info = Bundle.main.infoDictionary else {
            assert(false, "Failed while reading info from settings")
            return ""
        }
        if UrlConfigurator.isProduction() {
            guard let baseUrlPro = info["BaseUrlPro"] as? String else {
                assert(false, "Failed while reading BaseUrlPro from settings")
                return ""
            }
            return baseUrlPro
        } else {
            guard let baseUrlDev = info["BaseUrlDev"] as? String else {
                assert(false, "Failed while reading BaseUrlDev from settings")
                return ""
            }
            return baseUrlDev
        }
    }
    
    class func mqttUrlString() -> String {
        guard let info = Bundle.main.infoDictionary else {
            assert(false, "Failed while reading info from settings")
            return ""
        }
        if UrlConfigurator.isProduction() {
            guard let mqttUrlPro = info["MqttUrlPro"] as? String else {
                assert(false, "Failed while reading MqttUrlPro from settings")
                return ""
            }
            return mqttUrlPro
        } else {
            guard let mqttUrlDev = info["MqttUrlDev"] as? String else {
                assert(false, "Failed while reading MqttUrlDev from settings")
                return ""
            }
            return mqttUrlDev
        }
    }
    
    class func socketUrlString() -> String {
        guard let info = Bundle.main.infoDictionary else {
            assert(false, "Failed while reading info from settings")
            return ""
        }
        if UrlConfigurator.isProduction() {
            guard let socketUrlPro = info["SocketUrlPro"] as? String else {
                assert(false, "Failed while reading SocketUrlPro from settings")
                return ""
            }
            return socketUrlPro
        } else {
            guard let socketUrlDev = info["SocketUrlDev"] as? String else {
                assert(false, "Failed while reading SocketUrlDev from settings")
                return ""
            }
            return socketUrlDev
        }
    }
    
    class func urlByAdding(port:Int = 5525, path: String = "") -> URL {
        assert(port > 0, "Incorrect port value")
        guard let url = URL(string: baseUrlString() + ":\(port)")  else {
            assert(false, "Failed while constructing URL")
            return URL.invalidURL
        }
        return url.appendingPathComponent(path)
    }
    
}

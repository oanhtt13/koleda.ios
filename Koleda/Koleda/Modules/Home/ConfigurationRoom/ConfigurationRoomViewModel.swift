//
//  ConfigurationRoomViewModel.swift
//  Koleda
//
//  Created by Oanh tran on 8/26/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import UIKit
import RxSwift

protocol ConfigurationRoomViewModelProtocol: BaseViewModelProtocol {
    var title: Variable<String> { get }
    var roomName: Variable<String> { get }
    var temperature: Variable<String> { get }
    var pairedWithSensorTitle: Variable<String> { get }
    var pairedWithHeatersTitle: Variable<String> { get }
    var enableHeaterManagement: PublishSubject<Bool> { get }
    var imageRoom: PublishSubject<UIImage> { get }
    var heaters: Variable<[Heater]>  { get }
    var imageRoomColor : PublishSubject<UIColor> { get }
    func setup()
    func editRoom()
    func sensorManagement()
    func heatersManagement()
    func needUpdateSelectedRoom()
}

class ConfigurationRoomViewModel: BaseViewModel, ConfigurationRoomViewModelProtocol {
    let title = Variable<String>("")
    let roomName = Variable<String>("")
    var temperature = Variable<String>("")
    let pairedWithSensorTitle = Variable<String>("")
    let pairedWithHeatersTitle = Variable<String>("")
    let enableHeaterManagement = PublishSubject<Bool>()
    let imageRoom = PublishSubject<UIImage>()
    let imageRoomColor = PublishSubject<UIColor>()
    var heaters = Variable<[Heater]>([])
    
    let router: BaseRouterProtocol
    private let roomManager: RoomManager
    private var seletedRoom: Room?
    
    init(router: BaseRouterProtocol, managerProvider: ManagerProvider = .sharedInstance, seletedRoom: Room? = nil) {
        self.router = router
        self.roomManager = managerProvider.roomManager
        self.seletedRoom = seletedRoom
        super.init(managerProvider: managerProvider)
    }
    
    func setup() {
        guard let userName = UserDataManager.shared.currentUser?.name, let room = seletedRoom else {
            return
        }
        let roomViewModel = RoomViewModel.init(room: room)
        title.value = "\(userName)’s \(roomViewModel.roomName)"
        roomName.value = roomViewModel.roomName
        if let image = roomViewModel.roomConfigurationImage {
            imageRoom.onNext(image)
        }
        imageRoomColor.onNext(roomViewModel.onOffSwitchStatus ? UIColor.black : UIColor.gray)
        
        guard let sensor = roomViewModel.sensor else {
            pairedWithSensorTitle.value = "\(roomViewModel.roomName) is not connect to any sensor."
            pairedWithHeatersTitle.value = ""
            enableHeaterManagement.onNext(false)
            return
        }
        if let temp = roomViewModel.currentTemp {
            temperature.value = "\(temp)"
        } else {
            temperature.value = "-"
        }
        
        enableHeaterManagement.onNext(true)
        pairedWithSensorTitle.value = "Paired with \(roomViewModel.roomName) Sensor"
        guard let heaters = roomViewModel.heaters, heaters.count > 0 else {
            pairedWithHeatersTitle.value = "\(roomViewModel.roomName) Sensor isn't connecting to any heater."
            self.heaters.value = []
            return
        }
        self.heaters.value = heaters
        pairedWithHeatersTitle.value = "\(roomViewModel.roomName) Sensor is currently paired with these heaters"
    
    }
    
    func editRoom() {
        guard let room = seletedRoom else {
            return
        }
        router.enqueueRoute(with: ConfigurationRoomRouter.RouteType.editRoom(room))
    }
    
    func sensorManagement() {
        guard let room = seletedRoom else {
            return
        }
        guard let sensor = seletedRoom?.sensor else {
            router.enqueueRoute(with: ConfigurationRoomRouter.RouteType.addSensor(room.id, room.name))
            return
        }
        router.enqueueRoute(with: ConfigurationRoomRouter.RouteType.editSensor(room))
    }
    
    func heatersManagement() {
        guard let room = seletedRoom else {
            return
        }
        router.enqueueRoute(with: ConfigurationRoomRouter.RouteType.heaterManagement(room))
    }
    
    func needUpdateSelectedRoom() {
        if let roomId = seletedRoom?.id, let room = UserDataManager.shared.roomWith(roomId: roomId) {
            seletedRoom = room
            setup()
        }
    }
}

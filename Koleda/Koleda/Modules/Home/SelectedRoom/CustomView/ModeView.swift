//
//  ModeView.swift
//  Koleda
//
//  Created by Oanh tran on 9/9/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import UIKit

struct ModeItem {
    let mode: SmartMode
    let title: String
    let icon: UIImage?
    let temperature: Double
    let color: UIColor
    
    init(mode: SmartMode, title: String, icon: UIImage?, temp: Double) {
        self.mode = mode
        self.title = title
        self.icon = icon
        self.temperature = temp
        switch mode {
        case .ECO:
            self.color = UIColor.purpleLight
        case .NIGHT:
            self.color = UIColor.redLight
        case .COMFORT:
            self.color = UIColor.blueLight
        default:
            self.color = UIColor.yellowLight
        }
    }
    
    static func imageOf(smartMode: SmartMode) -> UIImage? {
        switch smartMode {
        case .ECO:
            return UIImage.init(named: "ecoMode")
        case .COMFORT:
            return UIImage.init(named: "comfortMode")
        case .NIGHT:
            return UIImage.init(named: "nightMode")
        default:
            return nil
        }
    }
    
    static func titleOf(smartMode: SmartMode) -> String {
        switch smartMode {
        case .ECO:
            return "ECO MODE"
        case .COMFORT:
            return "COMFORT MODE"
        case .NIGHT:
            return "NIGHT MODE"
        default:
            return ""
        }
    }
    
    static func modeNameOf(smartMode: SmartMode) -> String {
        switch smartMode {
        case .ECO:
            return "Eco mode"
        case .COMFORT:
            return "Comfort mode"
        case .NIGHT:
            return "Night mode"
        default:
            return ""
        }
    }
    
    static func getModeItem(with smartMode: SmartMode) -> ModeItem? {
        let modeItems = UserDataManager.shared.settingModes
        return modeItems.filter { $0.mode == smartMode}.first
    }
}

class ModeView: UIView {
    
    func setUp(modeItem: ModeItem) {
        self.modeItem = modeItem
        updateStatus(enable: false)
    }
    
    @IBOutlet weak var modeIconImageView: UIImageView!
    @IBOutlet weak var titleModeLabel: UILabel!
    @IBOutlet weak var lineLabel: UILabel!
    var modeItem: ModeItem?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        kld_loadContentFromNib()
    }
    
    func updateStatus(enable: Bool, fromModifyModesScreen: Bool = false) {
        guard let modeItem = modeItem else {
            return
        }
        self.titleModeLabel.text = modeItem.title
        self.modeIconImageView.image = modeItem.icon
        self.modeIconImageView.tintColor = (enable || fromModifyModesScreen) ? modeItem.color : UIColor.gray
        self.backgroundColor = enable ? UIColor.white : UIColor.clear
        self.titleModeLabel.textColor = enable ? UIColor.black : UIColor.lightGray
        self.lineLabel.backgroundColor = enable ? modeItem.color : UIColor.clear
    }
    
}

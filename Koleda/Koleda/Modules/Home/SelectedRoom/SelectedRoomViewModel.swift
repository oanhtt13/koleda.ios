//
//  SelectedRoomViewModel.swift
//  Koleda
//
//  Created by Oanh tran on 8/26/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import Foundation
import RxSwift

protocol SelectedRoomViewModelProtocol: BaseViewModelProtocol {
    var homeTitle: Variable<String> { get }
    var temperature: Variable<String> { get }
    var humidity: Variable<String> { get }
    var sensorBattery: Variable<String> { get }
    var adjustTemp: Variable<Int> { get }
    var adjustTempSmall: Variable<Int> { get }
    var canAdjustTemp: PublishSubject<Bool> { get }
    var turnOnRoom: PublishSubject<Bool> { get }
    
    var modeItems: Variable<[ModeItem]> { get }
    var ecoModeUpdate: PublishSubject<Bool> { get }
    var comfortModeUpdate: PublishSubject<Bool> { get }
    var nightModeUpdate: PublishSubject<Bool> { get }
    var editingTemprature: Variable<Double> { get }
    var settingType: SettingType { get }
    var heaters: [Heater] { get }
    func setup()
    func showConfigurationScreen()
    func showManualBoostScreen()
    func needUpdateSelectedRoom()
    func updateSettingMode(mode: SmartMode, completion: @escaping (Bool, SmartMode) -> Void)
    func changeSmartMode(seletedSmartMode: SmartMode)
    func turnOnOrOffRoom(completion: @escaping (_ isTurnOn: Bool, _ isSuccess: Bool) -> Void)
}

class SelectedRoomViewModel: BaseViewModel, SelectedRoomViewModelProtocol {
    
    
    let homeTitle = Variable<String>("")
    let temperature = Variable<String>("")
    let humidity = Variable<String>("")
    let sensorBattery = Variable<String>("")
    let adjustTemp = Variable<Int>(0)
    let adjustTempSmall = Variable<Int>(0)
    let canAdjustTemp = PublishSubject<Bool>()
    var editingTemprature = Variable<Double>(0)
    let turnOnRoom = PublishSubject<Bool>()
    
    var modeItems = Variable<[ModeItem]>([])
    var ecoModeUpdate = PublishSubject<Bool>()
    var comfortModeUpdate = PublishSubject<Bool>()
    var nightModeUpdate = PublishSubject<Bool>()
    
    let router: BaseRouterProtocol
    private let roomManager: RoomManager
    private var seletedRoom: Room?
    private let settingManager: SettingManager
    private var currentSelectedMode: SmartMode = .DEFAULT
    var settingType: SettingType = .unknow
    var heaters: [Heater] = []
    init(router: BaseRouterProtocol, managerProvider: ManagerProvider = .sharedInstance, seletedRoom: Room? = nil) {
        self.router = router
        roomManager = managerProvider.roomManager
        settingManager =  managerProvider.settingManager
        self.seletedRoom = seletedRoom
        super.init(managerProvider: managerProvider)
    }
    
    func setup() {
        guard let userName = UserDataManager.shared.currentUser?.name, let room = seletedRoom else {
            return
        }
        
        let roomViewModel = RoomViewModel.init(room: room)
        turnOnRoom.onNext(roomViewModel.onOffSwitchStatus)
        homeTitle.value = "\(userName)’s \(roomViewModel.roomName)"
        temperature.value = roomViewModel.temprature
        humidity.value =  roomViewModel.humidity
        sensorBattery.value = roomViewModel.sensorBattery
        settingType = roomViewModel.settingType
        heaters = roomViewModel.heaters ?? []
        
        editingTemprature.value = Double.valueOf(clusterValue: roomViewModel.settingTemprature)
	if let heater = roomViewModel.heaters, heater.count > 0 {
            canAdjustTemp.onNext(true)
        } else {
            canAdjustTemp.onNext(false)
        }
        modeItems.value = UserDataManager.shared.settingModes
        if settingType == .SCHEDULE {
            changeSmartMode(seletedSmartMode: .DEFAULT)
        } else {
            changeSmartMode(seletedSmartMode: roomViewModel.smartMode)
        }
        
    }
    
    func turnOnOrOffRoom(completion: @escaping (_ isTurnOn: Bool, _ isSuccess: Bool) -> Void) {
        guard let room = seletedRoom else {
            completion(false, false)
            return
        }
        let roomViewModel = RoomViewModel.init(room: room)
        let status = !roomViewModel.onOffSwitchStatus
        roomManager.switchSensor(roomId: room.id, turnOn: status, success: {
            NotificationCenter.default.post(name: .KLDDidChangeRooms, object: nil)
            completion(status, true)
        }, failure: { _ in
            completion(status, false)
        })
    }
    
    func showConfigurationScreen() {
        guard let room = seletedRoom else {
            return
        }
        self.router.enqueueRoute(with: SelectedRoomRouter.RouteType.configuration(room))
    }
    
    func needUpdateSelectedRoom() {
        let room = UserDataManager.shared.rooms.filter { $0.id == seletedRoom?.id }.first
        if room != nil {
            seletedRoom = room
            setup()
        }
    }
    
    func showManualBoostScreen() {
        guard let room = seletedRoom else {
            return
        }
        self.router.enqueueRoute(with: SelectedRoomRouter.RouteType.manualBoost(room))
    }
    
    func changeSmartMode(seletedSmartMode: SmartMode) {
        currentSelectedMode = seletedSmartMode
        switch seletedSmartMode {
        case .ECO:
            ecoModeUpdate.onNext(true)
            comfortModeUpdate.onNext(false)
            nightModeUpdate.onNext(false)
        case .COMFORT:
            ecoModeUpdate.onNext(false)
            comfortModeUpdate.onNext(true)
            nightModeUpdate.onNext(false)
        case .NIGHT:
            ecoModeUpdate.onNext(false)
            comfortModeUpdate.onNext(false)
            nightModeUpdate.onNext(true)
        default:
            ecoModeUpdate.onNext(false)
            comfortModeUpdate.onNext(false)
            nightModeUpdate.onNext(false)
        }
    }
    
    func updateSettingMode(mode: SmartMode, completion: @escaping (Bool, SmartMode) -> Void) {
        var modeForUpdate: SmartMode = mode
        if currentSelectedMode != .DEFAULT && currentSelectedMode == mode {
            modeForUpdate = .DEFAULT
        }
        
        guard let roomId = seletedRoom?.id else {
            return
        }
        settingManager.updateSettingMode(mode: modeForUpdate.rawValue, roomId: roomId, success: {
            completion(true, modeForUpdate)
        }) { error in
            completion(false, modeForUpdate)
        }
    }
}

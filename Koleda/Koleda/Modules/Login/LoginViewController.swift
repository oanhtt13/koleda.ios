//
//  LoginViewController.swift
//  Koleda
//
//  Created by Oanh tran on 6/11/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import FacebookCore
import FacebookLogin
import GoogleSignIn
import SVProgressHUD

class LoginViewController: BaseViewController, BaseControllerProtocol, KeyboardAvoidable {
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var showPasswordButton: UIButton!
    @IBOutlet weak var loginGoogleButton: UIButton!
    @IBOutlet weak var loginFacebookButton: UIButton!
    @IBOutlet weak var emailTextField: AndroidStyle2TextField!
    @IBOutlet weak var passwordTextField: AndroidStyle2TextField!
    
    
    var viewModel: LoginViewModelProtocol!
    private let disposeBag = DisposeBag()
    var keyboardHelper: KeyboardHepler?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurationUI()
        keyboardHelper = KeyboardHepler(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        self.edgesForExtendedLayout = UIRectEdge.top
        self.automaticallyAdjustsScrollViewInsets = false
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        viewModel.prepare(for: segue)
    }
    
    @IBAction func showOrHidePass(_ sender: Any) {
        viewModel.showPassword(isShow: passwordTextField.isSecureTextEntry)
    }
    
    @IBAction func forgotPassword(_ sender: Any) {
        viewModel.forgotPassword()
    }
    
    @IBAction func login(_ sender: Any) {
        SVProgressHUD.show()
        loginButton.isEnabled = false
        viewModel.login { [weak self] isSuccess in
            SVProgressHUD.dismiss()
            self?.loginButton.isEnabled = true
            if !isSuccess {
                self?.app_showAlertMessage(title: "Login Failed", message: "Email or password is invalid")
            }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        back()
    }
    
    func loginUsingGoogle() {
        GIDSignIn.sharedInstance()?.uiDelegate = self
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    func loginUsingFacebook() {
        let loginFBManager = LoginManager()
        
        loginFBManager.logIn(permissions: [Permission.publicProfile, Permission.email], viewController: self) { [weak self] loginResult in
            switch loginResult {
            case .failed(let error):
                log.error(error.localizedDescription)
                self?.loginFacebookButton.isEnabled = true
                self?.app_showAlertMessage(title: "Login Facebook Failed", message: "We can't login by Facebook now. Please try again later")
            case .cancelled:
                log.info("User cancelled login.")
                self?.loginFacebookButton.isEnabled = true
                self?.app_showAlertMessage(title: "Login Facebook Failed", message: "We can't login by Facebook now. Please try again later")
            case .success(_, _, let fbAccessToken):
                log.info("logged in Facebook")
                SVProgressHUD.show()
                self?.viewModel.loginWithSocial(type: .facebook, accessToken: fbAccessToken.tokenString, completion: { [weak self] (isSuccess, error) in
                    self?.loginFacebookButton.isEnabled = true
                    SVProgressHUD.dismiss()
                    guard let error = error, !isSuccess else {
                        return
                    }
                    if error == WSError.emailNotExisted {
                        self?.app_showAlertMessage(title: "Login Facebook Failed", message: error.errorDescription)
                    } else {
                        self?.app_showAlertMessage(title: "Login Facebook Failed", message: "We can't login now. Please try again later")
                    }
                })
            }
        }
    }
}

extension LoginViewController {
    private func configurationUI() {
        viewModel?.showPassword.asObservable().subscribe(onNext: { [weak self]  value in
            self?.passwordTextField.isSecureTextEntry = !value
        }).disposed(by: disposeBag)
        
        viewModel.email.asObservable().bind(to: emailTextField.rx.text).disposed(by: disposeBag)
        emailTextField.rx.text.orEmpty.bind(to: viewModel.email).disposed(by: disposeBag)
        viewModel.password.asObservable().bind(to: passwordTextField.rx.text).disposed(by: disposeBag)
        passwordTextField.rx.text.orEmpty.bind(to: viewModel.password).disposed(by: disposeBag)
        
        viewModel.emailErrorMessage.asObservable().subscribe(onNext: { [weak self] message in
            self?.emailTextField.errorText = message
            if message.isEmpty {
                self?.emailTextField.showError(false)
            } else {
                self?.emailTextField.showError(true)
            }
        }).disposed(by: disposeBag)
        
        viewModel.passwordErrorMessage.asObservable().subscribe(onNext: { [weak self] message in
            self?.passwordTextField.errorText = message
            if message.isEmpty {
                self?.passwordTextField.showError(false)
            } else {
                self?.passwordTextField.showError(true)
            }
            
        }).disposed(by: disposeBag)
        
        loginGoogleButton.rx.tap.bind { [weak self] _ in
            self?.loginGoogleButton.isEnabled = false
            self?.loginUsingGoogle()
        }.disposed(by: disposeBag)
        
        loginFacebookButton.rx.tap.bind { [weak self] _ in
            self?.loginFacebookButton.isEnabled = false
            self?.loginUsingFacebook()
        }.disposed(by: disposeBag)
    }
}

extension LoginViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        if textField == passwordTextField {
            let currentText = textField.text as NSString?
            if let resultingText = currentText?.replacingCharacters(in: range, with: string) {
                return resultingText.count <= Constants.passwordMaxLength
            }
        }
        return true
    }
}

extension LoginViewController: GIDSignInDelegate, GIDSignInUIDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            loginGoogleButton.isEnabled = true
            log.error("\(error.localizedDescription)")
            app_showAlertMessage(title: "Login Google Failed", message: "We can't login by Google now. Please try again later")
        } else {
            // Perform any operations on signed in user here.
            SVProgressHUD.show()
            self.viewModel.loginWithSocial(type: .google, accessToken: user.authentication.idToken) { [weak self] (isSuccess, error) in
                self?.loginGoogleButton.isEnabled = true
                SVProgressHUD.dismiss()
                if !isSuccess {
                    self?.app_showAlertMessage(title: "Login Google Failed", message: "We can't login now. Please try again later")
                }
            }
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
}

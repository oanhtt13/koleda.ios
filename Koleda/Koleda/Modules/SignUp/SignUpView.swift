//
//  SignUpView.swift
//  Koleda
//
//  Created by Oanh tran on 6/3/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol SignUpViewDelegate: class {
    
}
class SignUpView: UIView {
    private var viewModel: SignUpViewModelProtocol?
    private weak var delegate: SignUpViewDelegate?
    
    @IBOutlet weak var nameTextField: AndroidStyleTextField!
    @IBOutlet weak var emailTextField: AndroidStyleTextField!
    @IBOutlet weak var passwordTextField: AndroidStyleTextField!
    @IBOutlet weak var passwordConfirmTextField: AndroidStyleTextField!
    @IBOutlet weak var showOrHidePassButton: UIButton!
    @IBOutlet weak var showOrHidePassConfirmButton: UIButton!
   
    var isHiddenPass: Bool = true
    var isHiddenPassConfirm: Bool = true
    private let disposeBag = DisposeBag()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        passwordTextField.accessoryView = showOrHidePassButton
        passwordConfirmTextField.accessoryView = showOrHidePassConfirmButton
    }
    
    func viewDidLoad() {
        configurationUI()
    }
    
    
    @IBAction func showOrHidePass(_ sender: Any) {
        viewModel?.showPassword(isConfirm: false, isShow: passwordTextField.isSecureTextEntry)
    }
    
    @IBAction func showOrHidePassConfirm(_ sender: Any) {
        viewModel?.showPassword(isConfirm: true, isShow: passwordConfirmTextField.isSecureTextEntry)
    }
    func setup(with viewModel: SignUpViewModelProtocol, delegate: SignUpViewDelegate) {
        self.viewModel = viewModel
        self.delegate = delegate
    }
}

extension SignUpView {
    private func configurationUI() {
        guard let viewModel = self.viewModel else {
            return
        }
    }
    
}

extension SignUpView: UITextFieldDelegate {
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool
    {
        if textField == nameTextField {
            let currentText = textField.text as NSString?
            if let resultingText = currentText?.replacingCharacters(in: range, with: string) {
                return resultingText.count <= Constants.nameMaxLength
            }
        }
        if textField == passwordTextField || textField == passwordConfirmTextField {
            let currentText = textField.text as NSString?
            if let resultingText = currentText?.replacingCharacters(in: range, with: string) {
                return resultingText.count <= Constants.passwordMaxLength
            }
        }
        return true
    }
}

//
//  SignUpViewModel.swift
//  Koleda
//
//  Created by Oanh tran on 6/4/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa


protocol SignUpViewModelProtocol: BaseViewModelProtocol {
    
    var fullName: Variable<String> { get }
    var email: Variable<String> { get }
    var password: Variable<String> { get }
    var passwordConfirm: Variable<String> { get }
    
    var showPassword: PublishSubject<Bool> { get }
    var showPasswordConfirm: PublishSubject<Bool> { get }
    
    var fullNameErrorMessage: Variable<String> { get }
    var emailErrorMessage: Variable<String> { get }
    var passwordErrorMessage: Variable<String> { get }
    var passwordConfirmErrorMessage: Variable<String> { get }
    var showErrorMessage: PublishSubject<String> { get }
    
    

    func showPassword(isConfirm: Bool, isShow: Bool)
    func next(completion: @escaping (WSError?) -> Void)
    func loginAfterSignedUp(completion: @escaping (String) -> Void)
    func goTermAndConditions()
}

class SignUpViewModel: BaseViewModel, SignUpViewModelProtocol {
    
    
    let router: BaseRouterProtocol
    
    let fullName = Variable<String>("")
    let email = Variable<String>("")
    let password = Variable<String>("")
    let passwordConfirm = Variable<String>("")
    
    let showPassword = PublishSubject<Bool>()
    let showPasswordConfirm = PublishSubject<Bool>()
    
    let fullNameErrorMessage = Variable<String>("")
    let emailErrorMessage =  Variable<String>("")
    let passwordErrorMessage =  Variable<String>("")
    let passwordConfirmErrorMessage = Variable<String>("")
    let showErrorMessage = PublishSubject<String>()
    
    private let signUpManager: SignUpManager
    private let loginAppManager: LoginAppManager
    
    init(router: BaseRouterProtocol, managerProvider: ManagerProvider = .sharedInstance) {
        self.router = router
        self.loginAppManager = managerProvider.loginAppManager
        self.signUpManager =  managerProvider.signUpManager
        super.init(managerProvider: managerProvider)
    }
    
    
    func showPassword(isConfirm: Bool, isShow: Bool) {
        if isConfirm {
            self.showPasswordConfirm.onNext(isShow)
        } else {
            self.showPassword.onNext(isShow)
        }
    }
    
    func next(completion: @escaping (WSError?) -> Void) {
        if validateAll() {
            let emailValue = email.value.extraWhitespacesRemoved
            let passwordValue = password.value.extraWhitespacesRemoved
            self.signUpManager.signUp(name: fullName.value.extraWhitespacesRemoved,
                                      email: emailValue,
                                      password: passwordValue,
            success: { [weak self] in
                completion(nil)
            },
            failure: { error in
                completion(error as? WSError)
            })
        }
    }
    
    func loginAfterSignedUp(completion: @escaping (String) -> Void) {
        let emailValue = email.value.extraWhitespacesRemoved
        let passwordValue = password.value.extraWhitespacesRemoved
        loginAppManager.login(email: emailValue, password: passwordValue, success: { [weak self] in
            completion("")
        }, failure: { error in
            completion("Login Failed, Please try again later.")
        })
    }
    
    func goTermAndConditions() {
        router.enqueueRoute(with: SignUpRouter.RouteType.termAndConditions)
    }
}

extension SignUpViewModel {
    
    
    
    private func validateAll() -> Bool {
        var failCount = 0
        failCount += validateFullName() ? 0 : 1
        failCount += validateEmail() ? 0 : 1
        failCount += validatePassword() ? 0 : 1
        return failCount == 0
    }
    
    private func validateFullName() -> Bool {
        let normalizedFullName = fullName.value.extraWhitespacesRemoved
        if normalizedFullName.isEmpty {
            fullNameErrorMessage.value = "Name is not Empty"
            return false
        }
        
        if DataValidator.isValid(fullName: normalizedFullName) && normalizedFullName.count >= 2 && normalizedFullName.count <= 50 {
            fullNameErrorMessage.value = ""
            return true
        } else {
            fullNameErrorMessage.value = "Name is InValid"
            return false
        }
    }
    
    private func validateEmail() -> Bool {
        if email.value.extraWhitespacesRemoved.isEmpty {
            emailErrorMessage.value = "Email is not Empty"
            return false
        }
        
        if DataValidator.isEmailValid(email: email.value.extraWhitespacesRemoved) {
            emailErrorMessage.value = ""
            return true
        } else {
            emailErrorMessage.value = "Email is invalid"
            return false
        }
    }
    
    private func validatePassword() -> Bool {
        if password.value.extraWhitespacesRemoved.isEmpty {
            passwordErrorMessage.value = "Password is not Empty"
            return false
        }
        
        if DataValidator.isEmailPassword(pass: password.value.extraWhitespacesRemoved)
        {
            passwordErrorMessage.value = ""
            return true
        } else {
            passwordErrorMessage.value = "INVALID_PASSWORD_MESSAGE".app_localized
            return false
        }
    }
    
    private func validatePasswordConfirm() -> Bool {
        if password.value.extraWhitespacesRemoved.isEmpty {
            passwordErrorMessage.value = "Password is not Empty"
            return false
        }
        
        if password.value == passwordConfirm.value {
            passwordConfirmErrorMessage.value = ""
            return true
        } else {
            passwordConfirmErrorMessage.value = "Password Confirm is not match"
            return false
        }
    }
}

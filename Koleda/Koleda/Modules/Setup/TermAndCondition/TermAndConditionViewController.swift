//
//  TermAndConditionViewController.swift
//  Koleda
//
//  Created by Oanh tran on 7/3/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import UIKit
import RxSwift
import SVProgressHUD
import WebKit

class TermAndConditionViewController: BaseViewController, BaseControllerProtocol, WKUIDelegate, WKNavigationDelegate {
    
    @IBOutlet weak var agreeButton: UIButton!
    @IBOutlet weak var webViewContainerView: UIView!
    private var webView: WKWebView?
    
    var viewModel: TermAndConditionViewModelProtocol!
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addWebView()
        agreeButton.isEnabled = false
        guard let url = URL(string: AppConstants.privacyPolicyLink), let webView = self.webView else {
            return
        }
        let requestObj = URLRequest(url: url as URL)
        SVProgressHUD.show(withStatus: "Loading...")
        webView.load(requestObj)
        
        agreeButton.rx.tap.bind { [weak self] _ in
            self?.viewModel.showLocationScreen()
        }.disposed(by: disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        addCloseFunctionality()
        setTitleScreen(with: "")
    }
    
    private func addWebView() {
        let webConfiguration = WKWebViewConfiguration()
        let size = CGSize.init(width: 0.0, height: self.webViewContainerView.frame.size.height)
        let customFrame = CGRect.init(origin: CGPoint.zero, size: size)
        self.webView = WKWebView (frame: customFrame, configuration: webConfiguration)
        if let webView = self.webView {
            webView.translatesAutoresizingMaskIntoConstraints = false
            self.webViewContainerView.addSubview(webView)
            webView.topAnchor.constraint(equalTo: webViewContainerView.topAnchor).isActive = true
            webView.rightAnchor.constraint(equalTo: webViewContainerView.rightAnchor).isActive = true
            webView.leftAnchor.constraint(equalTo: webViewContainerView.leftAnchor).isActive = true
            webView.bottomAnchor.constraint(equalTo: webViewContainerView.bottomAnchor).isActive = true
            webView.heightAnchor.constraint(equalTo: webViewContainerView.heightAnchor).isActive = true
            webView.uiDelegate = self
            webView.navigationDelegate = self
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish loading Term")
        agreeButton.isEnabled = true
        SVProgressHUD.dismiss()
    }
}

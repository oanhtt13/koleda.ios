//
//  MenuSettingsViewController.swift
//  Koleda
//
//  Created by Oanh tran on 9/9/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import UIKit
import RxSwift
import MessageUI

class MenuSettingsViewController: BaseViewController, BaseControllerProtocol {
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameTextField: AndroidStyleTextField!
    @IBOutlet weak var amountView: UIView!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var settingItemsTableView: UITableView!
    @IBOutlet weak var reportBugsButton: UIButton!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    var viewModel: MenuSettingsViewModelProtocol!
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurationUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationBarTransparency()
        addCloseFunctionality()
        addLogoutButton()
        viewModel.viewWillAppear()
    }

    private func configurationUI() {
        Style.Button.primary.apply(to: reportBugsButton)
        userImageView.layer.cornerRadius = userImageView.frame.width / 2.0
        userImageView.layer.masksToBounds = true
        viewModel?.profileImage.drive(userImageView.rx.image).disposed(by: disposeBag)
        Style.View.shadowCornerWhite.apply(to: amountView)
        viewModel.settingItems.asObservable().bind { [weak self] modeItems in
            self?.settingItemsTableView.reloadData()
        }.disposed(by: disposeBag)
        
        viewModel.userName.asObservable().bind { [weak self] value in
            self?.userNameTextField.titleText = value
        }.disposed(by: disposeBag)
        
        viewModel.email.asObservable().bind { [weak self] value in
            self?.userNameTextField.text = value
        }.disposed(by: disposeBag)
        
        viewModel.energyConsumed.asObservable().bind { [weak self] value in
            self?.amountLabel.text = value
        }.disposed(by: disposeBag)

        reportBugsButton.rx.tap.bind {  [weak self] in
            self?.sendEmail()
        }.disposed(by: disposeBag)
        
    }
    
    override func logout() {
        viewModel.logOut()
    }
    
    private func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([AppConstants.supportEmail])
            mail.setSubject("Report bugs and problems")
            present(mail, animated: true, completion: nil)
        } else if let emailUrl = createEmailUrl(to: AppConstants.supportEmail, subject: "", body: "") {
            UIApplication.shared.open(emailUrl)
            // show failure alert
        }
    }
    
    private func createEmailUrl(to: String, subject: String, body: String) -> URL? {
        let subjectEncoded = subject.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        let bodyEncoded = body.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        
        let gmailUrl = URL(string: "googlegmail://co?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let outlookUrl = URL(string: "ms-outlook://compose?to=\(to)&subject=\(subjectEncoded)")
        let yahooMail = URL(string: "ymail://mail/compose?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let sparkUrl = URL(string: "readdle-spark://compose?recipient=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
        let defaultUrl = URL(string: "mailto:\(to)?subject=\(subjectEncoded)&body=\(bodyEncoded)")
        
        if let gmailUrl = gmailUrl, UIApplication.shared.canOpenURL(gmailUrl) {
            return gmailUrl
        } else if let outlookUrl = outlookUrl, UIApplication.shared.canOpenURL(outlookUrl) {
            return outlookUrl
        } else if let yahooMail = yahooMail, UIApplication.shared.canOpenURL(yahooMail) {
            return yahooMail
        } else if let sparkUrl = sparkUrl, UIApplication.shared.canOpenURL(sparkUrl) {
            return sparkUrl
        }
        
        return defaultUrl
    }
}

extension MenuSettingsViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

extension MenuSettingsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.settingItems.value.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SettingItemtableViewCell.get_identifier, for: indexPath) as? SettingItemtableViewCell else {
            log.error("Invalid cell type call")
            return UITableViewCell()
        }
        let menuItem = self.viewModel.settingItems.value[indexPath.row]
        cell.setup(menuItem: menuItem)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.selectedItem(at: indexPath.row)
    }
}


//
//  EditSensorViewController.swift
//  Koleda
//
//  Created by Oanh tran on 9/4/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import UIKit
import RxSwift
import SVProgressHUD

class EditSensorViewController: BaseViewController, BaseControllerProtocol {

    @IBOutlet weak var sensorNameTextField: AndroidStyle3TextField!
    @IBOutlet weak var pairedWithHeatersLabel: UILabel!
    @IBOutlet weak var sensorModelLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var batteryLabel: UILabel!
    @IBOutlet weak var isOnCharge: UILabel!
    @IBOutlet weak var deleteSensorButton: UIButton!
    @IBOutlet weak var nPairedHeaterImageView: UIImageView!
    
    var viewModel: EditSensorViewModelProtocol!
    
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurationUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationBarTransparency()
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func configurationUI() {
        sensorNameTextField.isEnabled = false
        viewModel.sensorName.asObservable().bind(to: sensorNameTextField.rx.text).disposed(by: disposeBag)
        sensorNameTextField.rx.text.orEmpty.bind(to: viewModel.sensorName).disposed(by: disposeBag)
        viewModel.pairedWithHeaters.asObservable().bind(to: pairedWithHeatersLabel.rx.text).disposed(by: disposeBag)
        viewModel.nPairedHeaters.asObservable().bind { [weak self] (nHeaters) in
            self?.nPairedHeaterImageView.isHidden = nHeaters == 0
            self?.nPairedHeaterImageView.image = UIImage(named: nHeaters > 1 ? "ic-connected-heaters" : "ic-connected-heater")
        }
        viewModel.sensorModel.asObservable().bind(to: sensorModelLabel.rx.text).disposed(by: disposeBag)
        viewModel.temperature.asObservable().bind(to: temperatureLabel.rx.text).disposed(by: disposeBag)
        viewModel.humidity.asObservable().bind(to: humidityLabel.rx.text).disposed(by: disposeBag)
        viewModel.battery.asObservable().bind(to: batteryLabel.rx.text).disposed(by: disposeBag)
        deleteSensorButton.rx.tap.bind { [weak self] in
            self?.showPopupToDeleteSensor()
        }.disposed(by: disposeBag)
    }
    
    func showPopupToDeleteSensor() {
        if let vc = getViewControler(withStoryboar: "Room", aClass: AlertConfirmViewController.self) {
            vc.onClickLetfButton = {
                self.showConfirmPopUp()
            }
            if let sensorName: String = viewModel.sensorName.value {
                vc.typeAlert = .deleteSensor(sensorName: sensorName)
            }
            showPopup(vc)
        }
    }
    
    private func showConfirmPopUp() {
        if let vc = getViewControler(withStoryboar: "Room", aClass: AlertConfirmViewController.self) {
            vc.onClickRightButton = {
                self.viewModel.deleteSensor(completion: { isSuccess in
                    SVProgressHUD.dismiss()
                    if isSuccess {
                        NotificationCenter.default.post(name: .KLDDidChangeRooms, object: nil)
                        self.app_showInfoAlert("Sensor deleted Successfully!", title: "Koleda", completion: {
                            self.viewModel.backToHome()
                        })
                    } else {
                        self.app_showInfoAlert("Sensor can't delete now")
                    }
                })
            }
            vc.typeAlert = .confirmDeleteSensor()
            showPopup(vc)
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        back()
    }
}

//
//  ForgotPasswordViewController.swift
//  Koleda
//
//  Created by Oanh tran on 6/20/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import UIKit
import RxSwift
import SVProgressHUD

class ForgotPasswordViewController: BaseViewController, BaseControllerProtocol, KeyboardAvoidable {
    
    var viewModel: ForgotPassWordViewModelProtocol!
    
    @IBOutlet weak var emailTextField: AndroidStyle2TextField!
    @IBOutlet weak var confirmEmailImageView: UIImageView!
    
    private let disposeBag = DisposeBag()
    var keyboardHelper: KeyboardHepler?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurationUI()
        keyboardHelper = KeyboardHepler(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        self.edgesForExtendedLayout = UIRectEdge.top
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    @IBAction func backAction(_ sender: Any) {
        back()
    }
    
    @IBAction func sendLinkAction(_ sender: UIButton) {
        sender.isEnabled = false
        SVProgressHUD.show()
        self.viewModel.getNewPassword(completion: { [weak self] success in
            sender.isEnabled = true
            SVProgressHUD.dismiss()
            if success {
                self?.app_showInfoAlert("Reset Password Successfull, Please check email to create new Password!", title: "Koleda", completion: {
                    self?.viewModel.router.dismiss(animated: true, context: nil, completion: nil)
                })
            }
        })
    }
    
}

extension ForgotPasswordViewController {
    func configurationUI() {
        viewModel.email.asObservable().bind(to: emailTextField.rx.text).disposed(by: disposeBag)
        emailTextField.rx.text.orEmpty.bind(to: viewModel.email).disposed(by: disposeBag)
        
        viewModel.email.asObservable().subscribe(onNext: { [weak self]  value in
            self?.confirmEmailImageView.isHidden = isEmpty(value)
        }).disposed(by: disposeBag)
        
        viewModel.emailErrorMessage.asObservable().subscribe(onNext: { [weak self] message in
            self?.emailTextField.errorText = message
            if message.isEmpty {
                self?.emailTextField.showError(false)
            } else {
                self?.emailTextField.showError(true)
            }
        }).disposed(by: disposeBag)
    }
}

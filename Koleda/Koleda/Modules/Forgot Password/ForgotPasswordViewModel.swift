//
//  ForgotPasswordViewModel.swift
//  Koleda
//
//  Created by Oanh tran on 6/20/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import Foundation
import RxSwift

protocol ForgotPassWordViewModelProtocol: BaseViewModelProtocol {
    
    var email: Variable<String> { get }
    var emailErrorMessage: PublishSubject<String> { get }
    func getNewPassword(completion: @escaping (Bool) -> Void)
}

class ForgotPasswordViewModel: BaseViewModel, ForgotPassWordViewModelProtocol {
    var router: BaseRouterProtocol
    
    let email = Variable<String>("")
    let emailErrorMessage = PublishSubject<String>()
    private let signUpManager: SignUpManager
    
    init(router: BaseRouterProtocol, managerProvider: ManagerProvider = .sharedInstance) {
        self.router = router
        self.signUpManager =  managerProvider.signUpManager
        super.init(managerProvider: managerProvider)
    }
    
    func getNewPassword(completion: @escaping (Bool) -> Void) {
        
        guard validateEmail() else {
            completion(false)
            return
        }
        signUpManager.resetPassword(email: email.value.extraWhitespacesRemoved,
        success: { [weak self] in
            completion(true)
        },
        failure: { error in
            completion(false)
        })
    }
}

extension ForgotPasswordViewModel {
    private func validateEmail() -> Bool {
        if email.value.extraWhitespacesRemoved.isEmpty {
            emailErrorMessage.onNext("Email is not Empty")
            return false
        }
        if DataValidator.isEmailValid(email: email.value.extraWhitespacesRemoved) {
            emailErrorMessage.onNext("")
            return true
        } else {
            emailErrorMessage.onNext("Email is invalid")
            return false
        }
    }
}

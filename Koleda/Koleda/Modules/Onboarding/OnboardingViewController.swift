//
//  OnboardingViewController.swift
//  Koleda
//
//  Created by Oanh tran on 5/23/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import UIKit
import RxSwift
import FacebookCore
import FacebookLogin
import GoogleSignIn
import SVProgressHUD


class OnboardingViewController: BaseViewController, BaseControllerProtocol {
    var viewModel: OnboardingViewModelProtocol!
    @IBOutlet weak var googleLoginButton: UIButton!
    @IBOutlet weak var facebookLoginButton: UIButton!
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBarTransparency()
        googleLoginButton.rx.tap.bind { [weak self] _ in
            self?.googleLoginButton.isEnabled = false
            self?.loginUsingGoogle()
        }.disposed(by: disposeBag)

        facebookLoginButton.rx.tap.bind { [weak self] _ in
            self?.facebookLoginButton.isEnabled = false
            self?.loginUsingFacebook()
        }.disposed(by: disposeBag)
    }
    
    func loginUsingGoogle() {
        GIDSignIn.sharedInstance()?.uiDelegate = self
        GIDSignIn.sharedInstance()?.delegate = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    func loginUsingFacebook() {
        let loginFBManager = LoginManager()
        loginFBManager.logOut()
        loginFBManager.logIn(permissions: [Permission.publicProfile, Permission.email], viewController: self) { [weak self] loginResult in
            switch loginResult {
            case .failed(let error):
                log.error(error.localizedDescription)
                self?.facebookLoginButton.isEnabled = true
                self?.app_showAlertMessage(title: "Login Facebook Failed", message: "We can't login by Facebook now. Please try again later")
            case .cancelled:
                log.info("User cancelled login.")
                self?.facebookLoginButton.isEnabled = true
                self?.app_showAlertMessage(title: "Login Facebook Failed", message: "We can't login by Facebook now. Please try again later")
            case .success(_, _, let fbAccessToken):
                log.info("logged in Facebook")
                SVProgressHUD.show()
                self?.viewModel.loginWithSocial(type: .facebook, accessToken: fbAccessToken.tokenString, completion: { (isSuccess, error) in
                        self?.facebookLoginButton.isEnabled = true
                    SVProgressHUD.dismiss()
                    guard let error = error, !isSuccess else {
                        return
                    }
                    if error == WSError.emailNotExisted {
                        self?.app_showAlertMessage(title: "Login Facebook Failed", message: error.errorDescription)
                    } else {
                        self?.app_showAlertMessage(title: "Login Facebook Failed", message: "We can't login now. Please try again later")
                    }
                })
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        viewModel.prepare(for: segue)
    }
 
    @IBAction func signUp(_ sender: Any) {
        viewModel.startSignUpFlow()
    }
    
    @IBAction func signIn(_ sender: Any) {
        viewModel.startSignInFlow()
    }
}

extension OnboardingViewController: GIDSignInDelegate, GIDSignInUIDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            googleLoginButton.isEnabled = true
            log.error("\(error.localizedDescription)")
            app_showAlertMessage(title: "Login Google Failed", message: "We can't login by Google now. Please try again later")
        } else {
            // Perform any operations on signed in user here.
            SVProgressHUD.show()
            self.viewModel.loginWithSocial(type: .google, accessToken: user.authentication.idToken) { [weak self] (isSuccess, error) in
                self?.googleLoginButton.isEnabled = true
                SVProgressHUD.dismiss()
                if !isSuccess {
                    self?.app_showAlertMessage(title: "Login Google Failed", message: "We can't login now. Please try again later")
                }
            }
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
}

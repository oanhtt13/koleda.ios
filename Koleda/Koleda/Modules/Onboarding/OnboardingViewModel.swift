//
//  OnboardingViewModel.swift
//  Koleda
//
//  Created by Oanh tran on 5/23/19.
//  Copyright © 2019 koleda. All rights reserved.
//

import Foundation
import UIKit

enum SocialType: String {
    case facebook = "facebook"
    case google = "google"
}

protocol OnboardingViewModelProtocol: BaseViewModelProtocol {
    func prepare(for segue: UIStoryboardSegue)
    func startSignUpFlow()
    func startSignInFlow()
    func loginWithSocial(type: SocialType, accessToken: String, completion: @escaping (Bool, WSError?) -> Void)
}

class OnboardingViewModel: BaseViewModel, OnboardingViewModelProtocol {
    let router: BaseRouterProtocol
    private let loginAppManager: LoginAppManager
    
    init(with router: BaseRouterProtocol, managerProvider: ManagerProvider = .sharedInstance) {
        self.router = router
        self.loginAppManager = managerProvider.loginAppManager
        super.init(managerProvider: managerProvider)
    }
    
    
    func prepare(for segue: UIStoryboardSegue) {
        router.prepare(for: segue)
    }
    
    func loginWithSocial(type: SocialType, accessToken: String, completion: @escaping (Bool, WSError?) -> Void) {
        loginAppManager.loginWithSocial(type: type, accessToken: accessToken,
                                        success: { [weak self] in
            log.info("User signed in successfully")
            self?.router.enqueueRoute(with: OnboardingRouter.RouteType.termAndConditions)
            completion(true, nil)
        }, failure: { error in
            completion(false, error as? WSError)
        })
    }
    
    func startSignUpFlow() {
        router.enqueueRoute(with: OnboardingRouter.RouteType.signUp)
    }
    
    func startSignInFlow() {
        router.enqueueRoute(with: OnboardingRouter.RouteType.logIn)
    }
}
